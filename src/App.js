import React, {Component} from 'react';
import request from 'request';
import _ from 'lodash';
import Env from './config/environment';

import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail';

import './App.css';

class App extends Component {
	constructor(props){
		super(props);

		this.state = {
			videos: [],
			selectedVideo: null
		};
		this.videoSearch('surf');
	}

	videoSearch(term) {
		request.get({
			url: 'https://www.googleapis.com/youtube/v3/search',
			qs: {
				key: Env.YOUTUBE_API_KEY,
				part: 'snippet',
				type: 'video',
				q: term,
				maxResults: 50
			}
		}, (e, r, b) => {
			let response = JSON.parse(b);
			let videos = response.items;
			this.setState({videos, selectedVideo: videos[0]});
		});
	}

	render() {
		const videoSearch = _.debounce((term) => {this.videoSearch(term)},300);
		return (
			<div>
				<SearchBar onInputChange={videoSearch}/>
				<div>
					<VideoDetail video={this.state.selectedVideo}/>
					<VideoList
						onVideoSelect={selectedVideo => this.setState({selectedVideo})}
						videos={this.state.videos}/>
				</div>
			</div>
		);
	}
}

export default App;
