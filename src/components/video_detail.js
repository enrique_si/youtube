import React, {Component} from 'react';

class VideoDetail extends Component {

	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		let video = this.props.video;
		if(!video) return (<div>Esperando...</div>);
		let id = video.id.videoId;
		let url = `https://www.youtube.com/embed/${id}`;
		return (
			<div className="video-detail col-md-8">
				<div className="embed-responsive embed-responsive-16by9">
					<iframe className="embed-responsive-item" src={url}></iframe>
				</div>
				<div className="details">
					<div>{video.snippet.title}</div>
					<div>{video.snippet.description}</div>
				</div>
			</div>
		);
	}

}

export default VideoDetail;