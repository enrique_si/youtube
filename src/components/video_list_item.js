import React, {Component} from 'react';

class VideoListItem extends Component {

	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		let video = this.props.video;
		return (
			<li onClick={() => this.props.onVideoSelect(video)} className="list-group-item">
				<div className="video-list media">
					<div className="media-left">
						<img alt={video.snippet.title} src={video.snippet.thumbnails.default.url} className="media-object"/>
					</div>
					<div className="media-body">
						<div className="media-heading">{video.snippet.title}</div>
					</div>
				</div>
			</li>
		);
	}

}

export default VideoListItem;