import React, {Component} from 'react';
import VideoListItem from './video_list_item';

class VideoList extends Component {

	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		let props = this.props;
		let items = props.videos.map(function (video) {
			return (
				<VideoListItem
					onVideoSelect={props.onVideoSelect}
					key={video.etag}
					video={video} />
			);
		});
		return (
			<div>
				<ul className="col-md-4 list-group">
					{items}
				</ul>
			</div>
		);
	}

}

export default VideoList;